/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.style;

/**
 * Where to place node annotations.
 */
public enum AnnotationPosition
{
    /**
     * The node annotation is displayed on the same line as the node text, e.g&#46; <code>node text (annotation)</code>.
     * <pre> sample tree
     * ├─── node (annotation)
     * ├─── another node (annotation)
     * ╰─── last node</pre>
     */
    Inline(false),

    /**
     * The node annotation is positioned in a new line following the node text.
     * <pre> sample tree
     * ├─── node
     * │    (annotation)
     * ├─── another node
     * │    (annotation)
     * ╰─── last node</pre>
     */
    NextLine(true),

    /**
     * Like {@link #Inline}, but all aligned to the same column.
     * <pre> sample tree
     * ├─── node         (annotation)
     * ├─── another node (annotation)
     * ╰─── last node    (annotation)</pre>
     */
    Aligned(false),

    /** Annotations are not printed even if present. */
    None(true),

    /**
     * Annotations are printed on the next line, but moved to the left as far as possible, while continuing the tree
     * structure to the left of the annotation. One space is added to separate the annotation from the tree structure.
     * Where a node has no children, the annotation is indented by the same amount, so that a uniform appearance is
     * achieved.
     * <pre> sample tree
     * │ (annotation)
     * ├─── node
     * │      (annotation)
     * ╰─── last node
     *        (annotation)</pre>
     */
    Indented(true),

    /**
     * Annotations are placed to the right of the node text (like {@link #Inline}), but all lines of a multi-line
     * annotation are aligned to begin on the same column. The tree structure is continued to the left of the
     * annotation. Indent defined by the tree style is preserved.
     * <pre> root (annotation line 1)
     * │    (annotation line 2)
     * ├─── n1   (annotation line 1)
     * │         (annotation line 2)
     * ├─── another node (annotation line 1)
     * │                 (annotation line 2)
     * ╰─── last node</pre>
     */
    InlineRight(false);

    //

    private final boolean startsOnNewLine;



    private AnnotationPosition(final boolean pStartsOnNewLine)
    {
        startsOnNewLine = pStartsOnNewLine;
    }



    public boolean isStartsOnNewLine()
    {
        return startsOnNewLine;
    }
}
