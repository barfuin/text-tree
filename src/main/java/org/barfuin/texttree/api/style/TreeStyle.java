/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.style;

import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.texttree.internal.TreeUtil;


/**
 * Describes a tree style, giving all the text elements needed for rendering. This class is useful for implementing
 * custom tree styles. Pre-defined tree styles are available in the {@link TreeStyles} class.
 * <p>The README on the <a href="https://gitlab.com/barfuin/text-tree#custom-tree-style">project's website</a> has
 * detailed information about how to define custom tree styles.</p>
 *
 * @see TreeStyles
 */
public class TreeStyle
{
    private final String junction;

    private final String indent;

    private final String blankIndent;

    private final String indentTrimmed;

    private final String blankIndentTrimmed;

    private final String lastJunction;

    private final String calloutStart;

    private final String calloutEnd;



    /**
     * Constructor. All arguments are required and the junction elements must be of equal length.
     *
     * @param pJunction the text fragment for an edge <i>with</i> a junction
     * @param pIndent the text fragment for an edge <i>without</i> a junction
     * @param pLastJunction the text fragment for a junction which is the last in its node (the last child of a
     * node)
     * @param pCalloutStart the text fragment printed in front of a callout, for example&nbsp;<code>"&lt;"</code>
     * @param pCalloutEnd the text fragment printed after the end of a callout, for example&nbsp;<code>"&gt;"</code>
     * @see TreeStyles
     */
    public TreeStyle(final String pJunction, final String pIndent, final String pLastJunction,
        final String pCalloutStart, final String pCalloutEnd)
    {
        junction = requireNonEmpty(pJunction, "Argument pJunction must not be null or empty");
        indent = requireNonEmpty(pIndent, "Argument pIndent must not be null or empty");
        blankIndent = generateBlankIndent(pIndent);
        indentTrimmed = TreeUtil.rtrim(pIndent);
        blankIndentTrimmed = generateBlankIndent(indentTrimmed);
        lastJunction = requireNonEmpty(pLastJunction, "Argument pLastJunction must not be null or empty");
        calloutStart = Objects.requireNonNull(pCalloutStart, "Argument pCalloutStart must not be null");
        calloutEnd = Objects.requireNonNull(pCalloutEnd, "Argument pCalloutEnd must not be null");

        if (pJunction.length() != pLastJunction.length()) {
            throw new IllegalArgumentException("element size mismatch");
        }
    }



    /**
     * Constructor. All arguments are required and the junction elements must be of equal length. Callouts will be
     * enclosed in angle brackets.
     *
     * @param pJunction the text fragment for an edge <i>with</i> a junction
     * @param pIndent the text fragment for an edge <i>without</i> a junction
     * @param pLastJunction the text fragment for a junction which is the last in its node (the last child of a
     * node)
     * @see TreeStyles
     */
    public TreeStyle(final String pJunction, final String pIndent, final String pLastJunction)
    {
        this(pJunction, pIndent, pLastJunction, "<", ">");
    }



    private String requireNonEmpty(@Nullable final String pString, @Nonnull final String pMessage)
    {
        final String result = Objects.requireNonNull(pString, pMessage);
        if (result.isEmpty()) {
            throw new IllegalArgumentException(pMessage);
        }
        return result;
    }



    @Nonnull
    @SuppressWarnings("ReplaceAllDot")
    private String generateBlankIndent(@Nonnull final String pIndent)
    {
        return pIndent.replaceAll(".", " ");
    }



    @Nonnull
    public String getIndent()
    {
        return indent;
    }



    @Nonnull
    public String getBlankIndent()
    {
        return blankIndent;
    }



    @Nonnull
    public String getIndentTrimmed()
    {
        return indentTrimmed;
    }



    @Nonnull
    public String getBlankIndentTrimmed()
    {
        return blankIndentTrimmed;
    }



    @Nonnull
    public String getJunction()
    {
        return junction;
    }



    @Nonnull
    public String getLastJunction()
    {
        return lastJunction;
    }



    @Nonnull
    public String getCalloutStart()
    {
        return calloutStart;
    }



    @Nonnull
    public String getCalloutEnd()
    {
        return calloutEnd;
    }
}
