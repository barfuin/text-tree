/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

import java.util.EnumMap;
import java.util.Objects;
import java.util.function.BiFunction;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.texttree.api.color.ColorScheme;
import org.barfuin.texttree.api.color.DefaultColorScheme;
import org.barfuin.texttree.api.color.NoColorScheme;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.barfuin.texttree.api.style.TreeStyle;
import org.barfuin.texttree.api.style.TreeStyles;


/**
 * Configuration options that apply to one complete rendered tree.
 */
public final class TreeOptions
{
    private static final IdentityScheme DEFAULT_IDENTITY_SCHEME = IdentityScheme.ByIdentity;

    private static final TreeStyle DEFAULT_TREE_STYLE = TreeStyles.ASCII_ROUNDED;

    private static final CycleProtection DEFAULT_CYCLE_PROTECTION = CycleProtection.On;

    private static final ColorScheme NO_COLOR_SCHEME = new NoColorScheme();

    private static final AnnotationPosition DEFAULT_ANNOT_POS = AnnotationPosition.NextLine;

    private static final int DEFAULT_MAX_DEPTH = 100;

    private static final int DEFAULT_VERT_SPACING = 0;

    private IdentityScheme identityScheme = DEFAULT_IDENTITY_SCHEME;

    private TreeStyle style = DEFAULT_TREE_STYLE;

    private CycleProtection cycleProtection = DEFAULT_CYCLE_PROTECTION;

    private ColorScheme colorScheme = NO_COLOR_SCHEME;

    private AnnotationPosition annotationPosition = DEFAULT_ANNOT_POS;

    private int maxDepth = DEFAULT_MAX_DEPTH;

    private boolean cycleAsPruned = false;

    private int verticalSpacing = DEFAULT_VERT_SPACING;

    private EnumMap<Callout, String> calloutOverrides = new EnumMap<>(Callout.class);

    private BiFunction<Callout, Node, String> calloutProcessor = null;



    @Nonnull
    public IdentityScheme getIdentityScheme()
    {
        return identityScheme;
    }



    public void setIdentityScheme(@Nullable final IdentityScheme pIdentityScheme)
    {
        identityScheme = pIdentityScheme != null ? pIdentityScheme : DEFAULT_IDENTITY_SCHEME;
    }



    @Nonnull
    public TreeStyle getStyle()
    {
        return style;
    }



    public void setStyle(@Nullable final TreeStyle pStyle)
    {
        style = pStyle != null ? pStyle : DEFAULT_TREE_STYLE;
    }



    @Nonnull
    public CycleProtection getCycleProtection()
    {
        return cycleProtection;
    }



    public void setCycleProtection(@Nullable final CycleProtection pCycleProtection)
    {
        cycleProtection = pCycleProtection != null ? pCycleProtection : DEFAULT_CYCLE_PROTECTION;
    }



    /**
     * Setter.
     *
     * @param pEnableDefaultColoring flag indicating whether the resulting tree should have default colors
     * (<code>true</code>) or not be colored at all (<code>false</code>). The default is <code>false</code>.
     * Individual node colors will <em>always</em> be displayed if set.
     * @see #setColorScheme
     */
    public void setEnableDefaultColoring(final boolean pEnableDefaultColoring)
    {
        colorScheme = pEnableDefaultColoring ? new DefaultColorScheme() : NO_COLOR_SCHEME;
    }



    @Nonnull
    public ColorScheme getColorScheme()
    {
        return colorScheme;
    }



    public void setColorScheme(@Nullable final ColorScheme pColorScheme)
    {
        colorScheme = pColorScheme != null ? pColorScheme : NO_COLOR_SCHEME;
    }



    @Nonnull
    public AnnotationPosition getAnnotationPosition()
    {
        return annotationPosition;
    }



    public void setAnnotationPosition(@Nullable final AnnotationPosition pAnnotationPosition)
    {
        annotationPosition = pAnnotationPosition != null ? pAnnotationPosition : DEFAULT_ANNOT_POS;
    }



    public int getMaxDepth()
    {
        return maxDepth;
    }



    /**
     * Setter.
     *
     * @param pMaxDepth the maximum depth of the resulting tree. Nodes which are lower down will be clipped. A value of
     * zero indicates unlimited depth.
     */
    public void setMaxDepth(final int pMaxDepth)
    {
        if (pMaxDepth < 0) {
            throw new IllegalArgumentException("max depth cannot be negative");
        }
        maxDepth = pMaxDepth;
    }



    public boolean isCycleAsPruned()
    {
        return cycleAsPruned;
    }



    /**
     * Setter.
     *
     * @param pCycleAsPruned flag indicating whether detected cycles should be flagged with a
     * {@link Callout#RepeatingNode RepeatingNode} callout instead of a {@link Callout#Cycle Cycle}. This would tune
     * down the cycles a bit, a behavior which may be desired by the invoking application.
     */
    public void setCycleAsPruned(final boolean pCycleAsPruned)
    {
        cycleAsPruned = pCycleAsPruned;
    }



    public int getVerticalSpacing()
    {
        return verticalSpacing;
    }



    /**
     * Setter.
     *
     * @param pVerticalSpacing the number of blank lines to print before each node (defaults to 0)
     */
    public void setVerticalSpacing(final int pVerticalSpacing)
    {
        if (pVerticalSpacing < 0) {
            throw new IllegalArgumentException("vertical spacing cannot be negative");
        }
        verticalSpacing = pVerticalSpacing;
    }



    /**
     * Register a callout text for a callout type, which will be printed instead of the normal callout text.
     * <p>Note that callout texts produced by a callout processor take precedence over texts registered with this
     * method.</p>
     *
     * @param pCalloutType the callout type
     * @param pText the text to display for that callout, or <code>null</code> to show the default text
     * @see #setCalloutProcessor
     */
    public void setCalloutText(@Nonnull final Callout pCalloutType, @Nullable final String pText)
    {
        calloutOverrides.put(pCalloutType, pText);
    }



    /**
     * Look up the callout type in order to check if a specific callout text is registered for this callout type.
     * If so, return that text.
     *
     * @param pCalloutType the type of callout to check
     * @return the callout text, or <code>null</code> if no text was registered
     */
    @CheckForNull
    public String getCalloutText(@Nonnull final Callout pCalloutType)
    {
        return calloutOverrides.get(pCalloutType);
    }



    /**
     * Registers a <i>callout processor</i>. A callout processor is a lambda function which is passed the callout type
     * and the node for which the callout shall be displayed and returns a String with the callout text. The function
     * may also choose to return <code>null</code>, in which case no callout will be printed.
     * <p>The callout processor can only affect the callout texts, not other behavior. For example, when a cycle is
     * detected and the callout processor returns <code>null</code>, then no callout will be printed, but printing of
     * child nodes will still stop because of the cycle.</p>
     * <p>The node passed to the callout processor is the real object from your tree, which should be treated as
     * read-only, i.e. the callout processor should not modify the node.</p>
     * <p>The callout processor may refer to the text that would have been printed without it by calling
     * {@link Callout#getText()} on its first argument.</p>
     * <p>This method allows for highly dynamic callout texts which depend on the individual node as well as the callout
     * type. For simpler cases, consider using {@link #setCalloutText} which simply sets a text per callout type.</p>
     * <p>For example, in order to disable printing of callouts entirely, <code>setCalloutProcessor((c, n) -&gt;
     * null);</code></p>
     * <p>A callout processor takes precedence over callout texts registered with {@link #setCalloutText}.</p>
     *
     * @param pCalloutProcessor the callout processor, or <code>null</code> to unregister
     * @see #setCalloutText
     */
    public void setCalloutProcessor(@Nullable final BiFunction<Callout, Node, String> pCalloutProcessor)
    {
        calloutProcessor = pCalloutProcessor;
    }



    /**
     * Getter.
     *
     * @return the callout processor registered with {@link #setCalloutProcessor}, if any
     */
    @CheckForNull
    public BiFunction<Callout, Node, String> getCalloutProcessor()
    {
        return calloutProcessor;
    }



    public static TreeOptions copyOf(@Nonnull final TreeOptions pTreeOptions)
    {
        Objects.requireNonNull(pTreeOptions, "pTreeOptions must not be null");
        TreeOptions result = new TreeOptions();
        result.setIdentityScheme(pTreeOptions.getIdentityScheme());
        result.setStyle(pTreeOptions.getStyle());
        result.setCycleProtection(pTreeOptions.getCycleProtection());
        result.setColorScheme(pTreeOptions.getColorScheme());
        result.setAnnotationPosition(pTreeOptions.getAnnotationPosition());
        result.setMaxDepth(pTreeOptions.getMaxDepth());
        result.setCycleAsPruned(pTreeOptions.isCycleAsPruned());
        result.setVerticalSpacing(pTreeOptions.getVerticalSpacing());
        result.calloutOverrides = new EnumMap<>(pTreeOptions.calloutOverrides);
        result.setCalloutProcessor(pTreeOptions.getCalloutProcessor());
        return result;
    }
}
