/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

import java.util.Locale;
import java.util.ResourceBundle;
import javax.annotation.Nonnull;


/**
 * The possible callouts. Callouts are not annotations, but extra information added by the tree rendering algorithm.
 */
public enum Callout
{
    /** no callout is displayed */
    None,

    /** a subtree that was already printed before was pruned this time */
    RepeatingNode,

    /** a cycle was detected, i.e. a node was encountered who is a parent of itself */
    Cycle,

    /** the maximum configured tree depth was exceeded */
    MaxDepth;



    /**
     * Get the text of the callout.
     *
     * @return the callout text
     */
    @Nonnull
    public String getText()
    {
        if (this == None) {
            return "";
        }
        return ResourceBundle.getBundle("org.barfuin.texttree.internal.messages")
            .getString("callout." + name().toLowerCase(Locale.ENGLISH));
    }
}
