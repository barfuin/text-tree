/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;


/**
 * Utility class with static helper methods for internal use by text-tree classes.
 */
public final class TreeUtil
{
    private static final Pattern RTRIM_PATTERN = Pattern.compile("\\s+$");

    /** <img src="doc-files/TreeUtil-1.png" /> */
    private static final Pattern ANSI_COLOR_ESCAPES_PATTERN = Pattern.compile("\\x1b\\[[0-9;]*m");



    private TreeUtil()
    {
        // utility class
    }



    /**
     * Remove all whitespace from the right side of a String.
     *
     * @param pString any String
     * @return a new, trimmed String, or <code>null</code> if the input was <code>null</code>
     */
    public static String rtrim(@Nullable final String pString)
    {
        String result = null;
        if (pString != null) {
            result = RTRIM_PATTERN.matcher(pString).replaceAll("");
        }
        return result;
    }



    /**
     * Determine the length of a String in characters which are not ANSI color escapes. This usually amounts to the
     * real length of the printable String, although other non-printable characters might be present in the String,
     * which would skew the result. But we are explicitly targeting ANSI color escape codes.
     *
     * @param pString any String
     * @return its length in characters without any ANSI color escape codes
     */
    public static int lengthWithoutEsc(@Nullable final String pString)
    {
        int result = 0;
        if (pString != null) {
            result = pString.length();
            final Matcher matcher = ANSI_COLOR_ESCAPES_PATTERN.matcher(pString);
            while (matcher.find()) {
                result -= matcher.end() - matcher.start();
            }
        }
        return result;
    }



    public static boolean containsEscapes(@Nullable final String pString)
    {
        if (pString != null) {
            return ANSI_COLOR_ESCAPES_PATTERN.matcher(pString).find();
        }
        return false;
    }
}
