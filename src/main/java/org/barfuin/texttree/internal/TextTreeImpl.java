/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import org.barfuin.texttree.api.Node;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.color.NodeColor;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.barfuin.texttree.api.style.TreeStyle;


/**
 * Renders a text tree.
 *
 * <p>This class is not thread-safe. Use a distinct instance in each thread, if you do multi-threaded tree generation
 * at all 😉.</p>
 */
@NotThreadSafe
public class TextTreeImpl
    implements TextTree
{
    private static final String SPACES = "                                                                          "
        + "                                                                                                         ";

    private final TreeOptions options;

    private ColorizableAppender appender = null;

    private CycleDetector cycleDetector = null;

    /**
     * the width in characters of the printed tree, without annotations. Only needed, and thus only filled, when
     * annotation position is {@link AnnotationPosition#Aligned Aligned}.
     */
    private int treeWidth = -1;



    public TextTreeImpl()
    {
        this(null);
    }



    public TextTreeImpl(@Nullable final TreeOptions pOptions)
    {
        options = pOptions != null ? pOptions : new TreeOptions();
    }



    @Nonnull
    @Override
    public String render(@Nullable final Node pNode)
    {
        init(pNode);

        if (pNode != null) {
            render("", 0, pNode, false);
        }
        else {
            appender.appendText(null, null);
        }
        return appender.finishUp();
    }



    private void init(@Nullable final Node pNode)
    {
        appender = new ColorizableAppender(options);
        cycleDetector = new CycleDetector(options);
        if (options.getAnnotationPosition() == AnnotationPosition.Aligned) {
            treeWidth = precalcTreeWidth(pNode);
        }
    }



    @SuppressWarnings("ConstantConditions")
    private void render(@Nonnull final String pPrefix, final int pLevel, @Nullable final Node pNode,
        final boolean pPrecalcWidth)
    {
        final TreeStyle style = options.getStyle();
        final int nodeTextLastLineLen = printNodeText(pPrefix, pNode);

        final CalloutInternal callout = cycleDetector.visit(pNode);
        if (callout != CalloutInternal.None) {
            printCallout(callout, pNode, pPrecalcWidth);
            cycleDetector.pop();
            return;
        }

        if (options.getAnnotationPosition().isStartsOnNewLine() || pNode == null || pNode.getAnnotation() == null) {
            appender.newLine();
        }

        final boolean hasChildren = hasChildren(pNode);
        if (pNode != null) {
            printAnnotation(pPrefix, nodeTextLastLineLen, pNode, hasChildren);
            printVerticalSpacing(pPrefix, hasChildren);

            if (hasChildren) {
                for (Iterator<? extends Node> iter = pNode.getChildren().iterator(); iter.hasNext(); ) {
                    final boolean maxDepthExceeded = options.getMaxDepth() > 0 && pLevel >= options.getMaxDepth();
                    final Node child = iter.next();

                    appender.append(TreeElementType.Edge, pPrefix);
                    appender.append(TreeElementType.Edge,
                        iter.hasNext() && !maxDepthExceeded ? style.getJunction() : style.getLastJunction());

                    if (maxDepthExceeded) {
                        printCallout(CalloutInternal.MaxDepth, child, pPrecalcWidth);
                        break;
                    }
                    else {
                        render(pPrefix + (iter.hasNext() ? style.getIndent() : style.getBlankIndent()),
                            pLevel + 1, child, pPrecalcWidth);
                    }
                }
            }
        }
        else {
            printVerticalSpacing(pPrefix, hasChildren);
        }
        cycleDetector.pop();
    }



    private void printVerticalSpacing(@Nonnull final String pPrefix, final boolean pHasChildren)
    {
        String prefix = pPrefix + (pHasChildren ? options.getStyle().getIndent() : "");
        String trimmed = TreeUtil.rtrim(prefix);
        if (!trimmed.isEmpty()) {
            for (int i = 0; i < options.getVerticalSpacing(); i++) {
                appender.append(TreeElementType.Edge, trimmed);
                appender.newLine();
            }
        }
    }



    private int printNodeText(@Nonnull final String pPrefix, @Nullable final Node pNode)
    {
        int lastLineLength = 0;
        final String text = pNode != null ? pNode.getText() : null;
        if (text != null) {
            final String[] lines = text.split(System.lineSeparator());
            for (int i = 0; i < lines.length; i++) {
                final String line = lines[i];
                lastLineLength = TreeUtil.lengthWithoutEsc(line);
                if (i > 0) {
                    appender.append(TreeElementType.Edge, pPrefix);
                }
                appender.appendText(line, pNode.getColor());
                if (i < lines.length - 1) {
                    appender.newLine();
                }
            }
        }
        else {
            appender.appendText(null, pNode != null ? pNode.getColor() : null);
            lastLineLength = "null".length();
        }
        return lastLineLength;
    }



    private void printCallout(@Nonnull final CalloutInternal pCallout, @Nullable final Node pNode,
        final boolean pPrecalcWidth)
    {
        boolean printCallout = !pPrecalcWidth;
        String calloutText = null;
        if (printCallout && options.getCalloutProcessor() != null) {
            calloutText = options.getCalloutProcessor().apply(pCallout.getApiCallout(), pNode);
            if (calloutText == null) {
                printCallout = false;
            }
        }

        if (printCallout) {
            if (calloutText == null) {
                calloutText = options.getCalloutText(pCallout.getApiCallout());
            }
            if (calloutText == null) {
                calloutText = pCallout.getText();
            }

            if (pCallout.isPrintOnSameLine()) {
                appender.appendText(" ", null);
            }

            final TreeStyle style = options.getStyle();
            appender.append(pCallout.getElementType(), style.getCalloutStart());
            appender.append(pCallout.getElementType(), calloutText);
            appender.append(pCallout.getElementType(), style.getCalloutEnd());
        }
        appender.newLine();
    }



    /**
     * Print a node's annotation if present.
     *
     * @param pPrefix the edge structure that must be printed before subsequent lines of multi-line annotations
     * @param pNodeTextLastLineLen the length in characters of the last line of the node text
     * @param pNode the node whose annotation shall be printed
     * @param pHasChildren flag indicating whether the node has child nodes or not
     */
    private void printAnnotation(@Nonnull final String pPrefix, final int pNodeTextLastLineLen,
        @Nonnull final Node pNode, final boolean pHasChildren)
    {
        final String annotation = pNode.getAnnotation();
        final NodeColor colorOverride = pNode.getAnnotationColor();
        final TreeStyle style = options.getStyle();

        if (annotation != null && !annotation.isEmpty() && options.getAnnotationPosition() != AnnotationPosition.None) {
            final String[] lines = annotation.trim().split(System.lineSeparator());
            for (int i = 0; i < lines.length; i++) {
                final String line = lines[i];

                if (options.getAnnotationPosition().isStartsOnNewLine() || i > 0) {
                    appender.append(TreeElementType.Edge, pPrefix);
                }

                if (options.getAnnotationPosition() == AnnotationPosition.Inline) {
                    if (i > 0) {
                        appender.append(TreeElementType.Annotation, "  ", colorOverride);  // continuation indent
                    }
                    else {
                        appender.append(TreeElementType.Text, " ");       // separate from node text on same line
                    }
                }

                else if (options.getAnnotationPosition() == AnnotationPosition.InlineRight) {
                    if (i > 0) {
                        appender.append(TreeElementType.Edge,
                            pHasChildren ? style.getIndent() : style.getBlankIndent());
                        int numSpaces = Math.max(0, pNodeTextLastLineLen - style.getBlankIndent().length() + 1);
                        appender.append(TreeElementType.Annotation, nSpaces(numSpaces));
                    }
                    else {
                        int numSpaces = Math.max(1, style.getBlankIndent().length() - pNodeTextLastLineLen);
                        appender.append(TreeElementType.Text, nSpaces(numSpaces));
                    }
                }

                else if (options.getAnnotationPosition() == AnnotationPosition.Aligned) {
                    int numSpaces = Math.max(1, treeWidth - appender.getCurrentLineLength() + 1);
                    if (i > 0 && pHasChildren) {
                        appender.append(TreeElementType.Edge, style.getIndent());
                        numSpaces -= style.getIndent().length();
                    }
                    appender.append(TreeElementType.Annotation, nSpaces(numSpaces), colorOverride);
                }

                else if (options.getAnnotationPosition() == AnnotationPosition.Indented) {
                    appender.append(TreeElementType.Edge,
                        pHasChildren ? style.getIndentTrimmed() : style.getBlankIndentTrimmed());
                    appender.append(TreeElementType.Annotation, " ", colorOverride);
                }

                appender.append(TreeElementType.Annotation, line, colorOverride);
                appender.newLine();
            }
        }
    }



    @SuppressWarnings("ConstantConditions")
    static boolean hasChildren(@Nullable final Node pNode)
    {
        boolean result = false;
        if (pNode != null && pNode.getChildren() != null) {
            Iterator<? extends Node> iter = pNode.getChildren().iterator();
            return iter != null && iter.hasNext();
        }
        return result;
    }



    private int precalcTreeWidth(@Nullable final Node pNode)
    {
        TreeOptions noAnnotations = TreeOptions.copyOf(options);
        noAnnotations.setAnnotationPosition(AnnotationPosition.None);
        noAnnotations.setColorScheme(null);

        TextTreeImpl textTree = new TextTreeImpl(noAnnotations);
        textTree.init(pNode);
        textTree.render("", 0, pNode, true);
        String bareTree = textTree.appender.finishUp();
        return longestLine(bareTree);
    }



    int longestLine(@Nonnull final String pMultilineString)
    {
        final boolean containsEscapes = TreeUtil.containsEscapes(pMultilineString);
        int result = -1;
        int startOfLine = 0;

        for (int nlPos = pMultilineString.indexOf(System.lineSeparator()); nlPos >= 0;
            nlPos = pMultilineString.indexOf(System.lineSeparator(), startOfLine)) //
        {
            int lineLength = nlPos - startOfLine;
            if (containsEscapes) {
                lineLength = TreeUtil.lengthWithoutEsc(pMultilineString.substring(startOfLine, nlPos));
            }
            if (lineLength > result) {
                result = lineLength;
            }
            startOfLine = nlPos + System.lineSeparator().length();
        }

        int lineLength = pMultilineString.length() - startOfLine;
        if (lineLength > result) {
            result = lineLength;
        }
        return result;
    }



    String nSpaces(final int pNumSpaces)
    {
        if (pNumSpaces <= SPACES.length()) {
            return SPACES.substring(0, pNumSpaces);
        }
        return Stream.generate(() -> " ").limit(pNumSpaces).collect(Collectors.joining());
    }
}
