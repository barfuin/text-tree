/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import javax.annotation.Nonnull;

import org.barfuin.texttree.api.Callout;


/**
 * The internal representation of {@link Callout}s, with added internal metadata.
 */
public enum CalloutInternal
{
    /** no callout is displayed - the metadata are dummy values */
    None(Callout.None, TreeElementType.CalloutError, false),

    /** a subtree that was already printed before was pruned this time */
    RepeatingNode(Callout.RepeatingNode, TreeElementType.CalloutNote, true),

    /** a cycle was detected, i.e. a node was encountered who is a parent of itself */
    Cycle(Callout.Cycle, TreeElementType.CalloutError, true),

    /** the maximum configured tree depth was exceeded */
    MaxDepth(Callout.MaxDepth, TreeElementType.CalloutNote, false);

    //

    private final Callout apiCallout;

    private final TreeElementType elementType;

    private final boolean printOnSameLine;



    private CalloutInternal(@Nonnull final Callout pApiCallout, @Nonnull final TreeElementType pElementType,
        final boolean pPrintOnSameLine)
    {
        apiCallout = pApiCallout;
        elementType = pElementType;
        printOnSameLine = pPrintOnSameLine;
    }



    @Nonnull
    public Callout getApiCallout()
    {
        return apiCallout;
    }



    @Nonnull
    public TreeElementType getElementType()
    {
        return elementType;
    }



    @Nonnull
    public String getText()
    {
        return apiCallout.getText();
    }



    public boolean isPrintOnSameLine()
    {
        return printOnSameLine;
    }
}
