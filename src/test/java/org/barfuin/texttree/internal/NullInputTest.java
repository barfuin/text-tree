/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.junit.Assert;
import org.junit.Test;


/**
 * Some unit tests of {@link TextTreeImpl} that test with input values set to <code>null</code>.
 */
public class NullInputTest
{
    @Test
    public void testNullInput()
    {
        final TextTree underTest = TextTree.newInstance();
        final String actual = underTest.render(null);
        Assert.assertEquals("null", actual);
    }



    @Test
    public void testNullInputChildren()
    {
        final TestNode tree = new TestNode("ROOT");
        tree.addChild(null);
        tree.addChild(null);
        final TextTree underTest = TextTree.newInstance();

        final String actual = underTest.render(tree);

        Assert.assertEquals(
            "ROOT" + System.lineSeparator()
                + "+--- null" + System.lineSeparator()
                + "`--- null" + System.lineSeparator(),   // nulls cannot constitute a cycle
            actual);
    }



    @Test
    public void testNullInputText()
    {
        final TestNode tree = new TestNode(null);
        TestNode child = new TestNode("child");
        child.setAnnotation(null);
        tree.addChild(child);
        final TextTree underTest = TextTree.newInstance();

        final String actual = underTest.render(tree);

        Assert.assertEquals("null" + System.lineSeparator() + "`--- child" + System.lineSeparator(), actual);
    }



    @Test
    public void testNullInputChildrenList()
    {
        final TestNode tree = new TestNode("ROOT");
        tree.setChildren(null);
        final TextTree underTest = TextTree.newInstance();

        final String actual = underTest.render(tree);

        Assert.assertEquals("ROOT" + System.lineSeparator(), actual);
    }



    @Test
    @SuppressWarnings("ConstantConditions")
    public void testNullInputChildrenIterator()
    {
        final TestNode tree = new TestNode("ROOT");
        tree.setChildren(() -> null);   // the children are not null, but return a null iterator -> nasty!
        final TextTree underTest = TextTree.newInstance();

        final String actual = underTest.render(tree);

        Assert.assertEquals("ROOT" + System.lineSeparator(), actual);
    }



    @Test
    public void testNullTreeAligned()
    {
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Aligned);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(null);

        Assert.assertEquals("null", actual);
    }



    @Test
    public void testVerticalSpacingWithNullNodeInList()
    {
        final TestNode tree = new TestNode("ROOT");
        tree.addChild(new TestNode("childA"));
        tree.addChild(null);
        tree.addChild(new TestNode("childC"));

        final TreeOptions options = new TreeOptions();
        options.setVerticalSpacing(1);
        final TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        Assert.assertEquals("ROOT" + System.lineSeparator()
                + "|" + System.lineSeparator()
                + "+--- childA" + System.lineSeparator()
                + "|" + System.lineSeparator()
                + "+--- null" + System.lineSeparator()
                + "|" + System.lineSeparator()
                + "`--- childC" + System.lineSeparator(),
            actual);
    }



    @Test
    public void testVerticalSpacingWithNullNodeAtEnd()
    {
        final TestNode tree = new TestNode("ROOT");
        tree.addChild(new TestNode("childA"));
        tree.addChild(new TestNode("childB"));
        tree.addChild(null);

        final TreeOptions options = new TreeOptions();
        options.setVerticalSpacing(1);
        final TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        Assert.assertEquals("ROOT" + System.lineSeparator()
                + "|" + System.lineSeparator()
                + "+--- childA" + System.lineSeparator()
                + "|" + System.lineSeparator()
                + "+--- childB" + System.lineSeparator()
                + "|" + System.lineSeparator()
                + "`--- null" + System.lineSeparator(),
            actual);
    }
}
