/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.TestTreeFactory;
import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.Callout;
import org.barfuin.texttree.api.CycleProtection;
import org.barfuin.texttree.api.IdentityScheme;
import org.barfuin.texttree.api.Node;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.barfuin.texttree.api.style.TreeStyles;
import org.junit.Assert;
import org.junit.Test;


public class CalloutOverridesTest
{
    private static class TestCalloutProcessor
        implements BiFunction<Callout, Node, String>
    {
        private final Map<String, Integer> counters = new HashMap<>();



        @Override
        public String apply(final Callout pCallout, final Node pNode)
        {
            if (pCallout == Callout.RepeatingNode) {
                final String key = pNode.getKey();
                int count = 1;
                if (counters.containsKey(key)) {
                    count = counters.get(key).intValue() + 1;
                }
                counters.put(key, Integer.valueOf(count));
                return "shown before, " + (count == 1 ? "once" : (count + " times"));
            }
            return pCallout.getText();
        }
    }



    /**
     * Test case which uses the callout processor feature to print how often a repeating node was pruned. This is a
     * somewhat contrived test case, but it shows the flexibility of a callout processor and how it can use the node
     * reference.
     *
     * @throws IOException bug in test case
     * @throws URISyntaxException bug in test case
     */
    @Test
    public void testRepetitionCount()
        throws IOException, URISyntaxException
    {
        final TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByKey);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Inline);
        options.setStyle(TreeStyles.UNICODE);
        options.setCalloutProcessor(new TestCalloutProcessor());
        Node tree = new TestTreeFactory().createManyRepeatingNodes();

        final String actual = TextTree.newInstance(options).render(tree);

        final String expected = TestUtil.file2String("internal/expected12-repeatCount.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testDisableCallouts()
        throws IOException, URISyntaxException
    {
        final TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByKey);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Inline);
        options.setStyle(TreeStyles.UNICODE);
        options.setCalloutProcessor((c, n) -> null);
        Node tree = new TestTreeFactory().createManyRepeatingNodes();

        final String actual = TextTree.newInstance(options).render(tree);

        final String expected = TestUtil.file2String("internal/expected12-noCallouts.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testCalloutOverrides()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createComplexTree();
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByText);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Inline);
        options.setStyle(TreeStyles.UNICODE_ROUNDED);
        options.setMaxDepth(4);
        options.setCalloutText(Callout.Cycle, "infinite loop");
        options.setCalloutText(Callout.MaxDepth, "depth exceeded");
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected13-calloutOverrides.txt");
        Assert.assertEquals(expected, actual);
    }
}
