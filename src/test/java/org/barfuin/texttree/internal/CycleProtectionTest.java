/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Locale;

import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.TestTreeFactory;
import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.CycleProtection;
import org.barfuin.texttree.api.IdentityScheme;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests of {@link TextTreeImpl} which specifically deal with cycle protection.
 */
public class CycleProtectionTest
{
    @Test
    public void testSimpleCycleByIdentity()
    {
        TestNode tree = new TestNode("A");
        TestNode nodeA = new TestNode("A");
        TestNode nodeB = new TestNode("A");
        tree.addChild(nodeA);
        nodeA.addChild(nodeB);
        nodeB.addChild(nodeA);
        TextTree underTest = TextTree.newInstance();

        final String actual = underTest.render(tree);

        final String expected =
            "A" + System.lineSeparator()
                + "`--- A" + System.lineSeparator()
                + "     `--- A" + System.lineSeparator()
                + "          `--- A <CYCLE>" + System.lineSeparator();
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleNoCycleByIdentity()
    {
        TestNode tree = new TestNode("ROOT");
        TestNode nodeA = new TestNode("A");
        TestNode nodeB = new TestNode("B");
        TestNode nodeA2 = new TestNode("A");  // separate object, same text!
        tree.addChild(nodeA);
        nodeA.addChild(nodeB);
        nodeB.addChild(nodeA2);
        TextTree underTest = TextTree.newInstance();   // default is byIdentity

        final String actual = underTest.render(tree);

        final String expected =
            "ROOT" + System.lineSeparator()
                + "`--- A" + System.lineSeparator()
                + "     `--- B" + System.lineSeparator()
                + "          `--- A" + System.lineSeparator();
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleCycleByText()
    {
        TestNode tree = new TestNode("ROOT");
        TestNode nodeA = new TestNode("A");
        TestNode nodeB = new TestNode("B");
        TestNode nodeA2 = new TestNode("A");  // separate object, same text!
        tree.addChild(nodeA);
        nodeA.addChild(nodeB);
        nodeB.addChild(nodeA2);
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByText);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected =
            "ROOT" + System.lineSeparator()
                + "`--- A" + System.lineSeparator()
                + "     `--- B" + System.lineSeparator()
                + "          `--- A <CYCLE>" + System.lineSeparator();
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleCycleByKey()
    {
        TestNode tree = new TestNode("ROOT");
        TestNode nodeA = new TestNode("A", "key1");
        TestNode nodeA2 = new TestNode("A", "key2"); // separate object, same text, different key
        TestNode nodeB = new TestNode("B", "key1");  // separate object, different text, same key -> cycle!
        tree.addChild(nodeA);
        nodeA.addChild(nodeA2);
        nodeA2.addChild(nodeB);
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByKey);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected =
            "ROOT" + System.lineSeparator()
                + "`--- A" + System.lineSeparator()
                + "     `--- A" + System.lineSeparator()
                + "          `--- B <CYCLE>" + System.lineSeparator();
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testComplexRepeatingByText()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createComplexTree();
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByText);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Inline);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-cycle-repeating1.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testComplexRepeatingByText2()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createComplexTree();
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByText);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Inline);
        options.setCycleAsPruned(true);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-cycle-repeating2.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testComplexRepeatingByTextColored()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createComplexTree();
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByText);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Inline);
        options.setEnableDefaultColoring(true);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-cycle-repeating1-defColor.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testComplexRepeatingByKey()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createComplexTree();
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByKey);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Inline);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-cycle-repeating1.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testIgnoreCycle()
    {
        TestNode tree = new TestNode("ROOT");
        TestNode nodeA = new TestNode("A");
        nodeA.addChild(nodeA);
        tree.addChild(nodeA);
        TreeOptions options = new TreeOptions();
        options.setMaxDepth(5);
        options.setCycleProtection(CycleProtection.Off);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected =
            "ROOT" + System.lineSeparator()
                + "`--- A" + System.lineSeparator()
                + "     `--- A" + System.lineSeparator()
                + "          `--- A" + System.lineSeparator()
                + "               `--- A" + System.lineSeparator()
                + "                    `--- A" + System.lineSeparator()
                + "                         `--- <snip>" + System.lineSeparator();
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testComplexGermanCallouts()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createComplexTree();
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByText);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Inline);
        options.setMaxDepth(4);
        TextTree underTest = TextTree.newInstance(options);

        String actual = "";
        try {
            Locale.setDefault(Locale.GERMANY);
            actual = underTest.render(tree);
        }
        finally {
            Locale.setDefault(Locale.ENGLISH);
        }

        final String expected = TestUtil.file2String("internal/expected-german-callouts.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testCyclePrunedTwice()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createTreeForCycleBug();
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByText);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-cycle-pruned.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testComplexRepeatingByTextAligned()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createTreeWithCallouts();
        TreeOptions options = new TreeOptions();
        options.setIdentityScheme(IdentityScheme.ByText);
        options.setCycleProtection(CycleProtection.PruneRepeating);
        options.setAnnotationPosition(AnnotationPosition.Aligned);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-cycle-repeating-aligned.txt");
        Assert.assertEquals(expected, actual);
    }
}
