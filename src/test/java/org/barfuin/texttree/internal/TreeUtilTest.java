/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import org.junit.Assert;
import org.junit.Test;


/**
 * Some unit tests of {@link TreeUtil}.
 */
public class TreeUtilTest
{
    @Test
    public void testNullInputs()
    {
        Assert.assertNull(TreeUtil.rtrim(null));
        Assert.assertEquals(0, TreeUtil.lengthWithoutEsc(null));
        Assert.assertFalse(TreeUtil.containsEscapes(null));
    }
}
