/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.io.IOException;
import java.net.URISyntaxException;

import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.TestTreeFactory;
import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.junit.Assert;
import org.junit.Test;


/**
 * Some unit tests for {@link TextTreeImpl} that specifically work with multi-line input text.
 */
public class MultilineTest
{
    @Test
    public void testSimpleTreeAsciiMultilineText()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createMultilineTree();
        TextTree underTest = TextTree.newInstance();

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected5-multilineNodeText.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeAsciiMultilineTextAnnotated()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createMultilineTree();
        annotateMultilineNodes(tree, false);
        tree.setAnnotation("annotation");  // annotation on root
        TextTree underTest = TextTree.newInstance();

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected5-multiline-annotated.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeAsciiMultilineTextAnnotatedInline()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createMultilineTree();
        annotateMultilineNodes(tree, true);
        tree.setAnnotation("(annotation)");  // annotation on root
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Inline);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected5-multiline-annotated-inline.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeAsciiMultilineTextAnnotatedAligned()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createMultilineTree();
        annotateMultilineNodes(tree, true);
        tree.setAnnotation("(annotation)");  // annotation on root
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Aligned);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected5-multiline-annotated-aligned.txt");
        Assert.assertEquals(expected, actual);
    }



    @SuppressWarnings("ConstantConditions")
    private void annotateMultilineNodes(final TestNode pNode, final boolean pAddParentheses)
    {
        Assert.assertNotNull(pNode);
        Assert.assertNotNull(pNode.getText());
        if (pNode.getText().startsWith("A in")) {
            pNode.setAnnotation(addParentheses("annotation", pAddParentheses));
        }
        else if (pNode.getText().startsWith("B21 in")) {
            pNode.setAnnotation(addParentheses("multi-line" + System.lineSeparator() + "annotation", pAddParentheses));
        }
        else if (pNode.getText().startsWith("C in")) {
            pNode.setAnnotation(
                addParentheses("more multi-line" + System.lineSeparator() + "annotation", pAddParentheses));
        }
        else if (pNode.getText().startsWith("C2 in")) {
            pNode.setAnnotation(addParentheses("annotation", pAddParentheses));
        }
        if (TextTreeImpl.hasChildren(pNode)) {
            pNode.getChildren().forEach(child -> annotateMultilineNodes(child, pAddParentheses));
        }
    }



    private String addParentheses(final String pString, final boolean pAddParentheses)
    {
        return pAddParentheses ? ("(" + pString + ")") : pString;
    }
}
