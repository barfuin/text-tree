/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.barfuin.texttree.AnnotationOnlyColorScheme;
import org.barfuin.texttree.NullColorScheme;
import org.barfuin.texttree.TestColorScheme;
import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.TestTreeFactory;
import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.color.NodeColor;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.fusesource.jansi.Ansi;
import org.junit.Assert;
import org.junit.Test;


/**
 * Some unit tests of {@link TextTreeImpl} which work specifically with colored tree elements.
 */
public class ColorTest
{
    @Test
    public void testSimpleTreeColoredNodes()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        setSomeNodesToRed(tree);
        TextTreeImpl underTest = new TextTreeImpl();

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-someColoredNodes.txt");
        Assert.assertEquals(expected, actual);
    }



    @SuppressWarnings("ConstantConditions")
    private void setSomeNodesToRed(final TestNode pNode)
    {
        if (pNode.getText() != null && pNode.getText().endsWith("2")) {
            pNode.setColor(NodeColor.LightRed);
        }
        if (TextTreeImpl.hasChildren(pNode)) {
            pNode.getChildren().forEach(this::setSomeNodesToRed);
        }
    }



    @Test
    public void testSimpleTreeAnnotatedDefaultColors()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TextTreeImplTest.annotateSomeNodes(tree);
        TreeOptions options = new TreeOptions();
        options.setEnableDefaultColoring(true);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-annotated-defColor.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeCustomColorScheme()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSmallTree();
        TreeOptions options = new TreeOptions();
        options.setColorScheme(new TestColorScheme());
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected4-customColors.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeCustomColorSchemeWithVerticalSpacing()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSmallTree();
        TreeOptions options = new TreeOptions();
        options.setColorScheme(new TestColorScheme());
        options.setVerticalSpacing(1);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected4-customColors-vspace1.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeNullColorScheme()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSmallTree();
        TreeOptions options = new TreeOptions();
        options.setColorScheme(new NullColorScheme());
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected4-noColors.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeAnnotatedColored()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TextTreeImplTest.annotateSomeNodes(tree);
        setSomeAnnotationsToRed(tree);
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Inline);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-annotated_colored-inline.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testTurnOffColoringForOneAnnotation()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TextTreeImplTest.annotateSomeNodes(tree);
        List<TestNode> children = (List<TestNode>) tree.getChildren();
        Assert.assertNotNull(children);
        children.get(1).setAnnotationColor(NodeColor.None);  // node 'B'
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Inline);
        options.setColorScheme(new AnnotationOnlyColorScheme());
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-annotated_colored-inline2.txt");
        Assert.assertEquals(expected, actual);
    }



    @SuppressWarnings("ConstantConditions")
    private void setSomeAnnotationsToRed(final TestNode pNode)
    {
        if (pNode.getText() != null && ("A".equals(pNode.getText()) || "B".equals(pNode.getText()))) {
            pNode.setAnnotationColor(NodeColor.LightRed);
        }
        if (TextTreeImpl.hasChildren(pNode)) {
            pNode.getChildren().forEach(this::setSomeAnnotationsToRed);
        }
    }



    @Test
    public void testPartiallyColoredText()
        throws IOException, URISyntaxException
    {
        Ansi ansi = Ansi.ansi();
        TestNode node1 = new TestNode(ansi.a("node1").fgBrightRed().a("RED").reset().a("normal").toString());
        node1.setAnnotation("annotation");
        TestNode node2 = new TestNode("node2");
        node2.setAnnotation("annotation");
        TestNode tree = new TestNode("ROOT");
        tree.setAnnotation("annotation");
        tree.addChild(node1);
        tree.addChild(node2);

        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Aligned);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected9-colorText.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testPartiallyColoredTextMultiLine()
        throws IOException, URISyntaxException
    {
        TestNode node1 = new TestNode(Ansi.ansi().a("node1").newline()
            .fgBrightRed().a("RED").reset().a(" normal").toString());
        node1.setAnnotation("(annotation)" + System.lineSeparator() + "(annotation)");
        TestNode node2 = new TestNode(Ansi.ansi().a("node2").newline()
            .a("also normal ").fgBrightRed().a("RED").reset().toString());
        node2.setAnnotation("(annotation)" + System.lineSeparator() + "(annotation)");
        TestNode tree = new TestNode(Ansi.ansi().reset().a("ROOT ").fgBrightCyan().a("NODE").toString());
        tree.setColor(NodeColor.LightCyan);
        tree.setAnnotation("(annotation)" + System.lineSeparator() + "(annotation)");
        tree.addChild(node1);
        tree.addChild(node2);

        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.InlineRight);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected10-colorText.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testPartiallyColoredAnnotation()
        throws IOException, URISyntaxException
    {
        TestNode node1 = new TestNode("node1");
        node1.setAnnotation(Ansi.ansi().a("(annotation1 ").fgBrightRed().a("RED").reset().a(" normal)").toString());
        TestNode node2 = new TestNode("node2");
        node2.setAnnotationColor(NodeColor.LightCyan);
        node2.setAnnotation(Ansi.ansi().a("(annotation1 ").reset().a("normal").fgBrightCyan().a(")").toString());
        TestNode tree = new TestNode("ROOT");
        tree.setAnnotation(Ansi.ansi().a("(annotation1 ").fgBrightRed().a("RED)").reset().toString());
        tree.addChild(node1);
        tree.addChild(node2);

        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Inline);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected11-coloredAnnotations.txt");
        Assert.assertEquals(expected, actual);
    }
}
