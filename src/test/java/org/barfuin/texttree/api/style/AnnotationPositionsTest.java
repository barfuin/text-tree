/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.style;

import java.io.IOException;
import java.net.URISyntaxException;

import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.TestTreeFactory;
import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests for different annotation positions.
 */
public class AnnotationPositionsTest
{
    @Test
    public void testIndented1()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        annotateNodes(tree);
        tree.setAnnotation("annotation");  // annotation on root
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Indented);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected5-multiline-annotated-indented1.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testIndented2()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        annotateNodes(tree);
        tree.setAnnotation("annotation");  // annotation on root
        TreeOptions options = new TreeOptions();
        options.setStyle(new TreeStyle("#+-- ", "##   ", "``-- "));
        options.setAnnotationPosition(AnnotationPosition.Indented);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected5-multiline-annotated-indented2.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testInlineRight()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        annotateNodes(tree);
        tree.setAnnotation("multi-line" + System.lineSeparator() + "annotation");  // annotation on root
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.InlineRight);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected5-multiline-annotated-inlineRight.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testInlineRightNodeLengths()
    {
        final String annotationText = "(annotation line 1)" + System.lineSeparator() + "(annotation line 2)";
        TestNode n1 = new TestNode("n1");
        n1.setAnnotation(annotationText);
        TestNode n2 = new TestNode("node");
        n2.setAnnotation(annotationText);
        TestNode n3 = new TestNode("node3");
        n3.setAnnotation(annotationText);
        TestNode n4 = new TestNode("another node");
        n4.setAnnotation(annotationText);
        TestNode tree = new TestNode("sample tree");
        tree.setAnnotation(annotationText);
        tree.addChild(n1);
        tree.addChild(n2);
        tree.addChild(n3);
        tree.addChild(n4);
        tree.addChild(new TestNode("last node"));

        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.InlineRight);
        options.setStyle(TreeStyles.UNICODE_ROUNDED);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = "sample tree (annotation line 1)" + System.lineSeparator()
            + "│           (annotation line 2)" + System.lineSeparator()
            + "├─── n1   (annotation line 1)" + System.lineSeparator()
            + "│         (annotation line 2)" + System.lineSeparator()
            + "├─── node (annotation line 1)" + System.lineSeparator()
            + "│         (annotation line 2)" + System.lineSeparator()
            + "├─── node3 (annotation line 1)" + System.lineSeparator()
            + "│          (annotation line 2)" + System.lineSeparator()
            + "├─── another node (annotation line 1)" + System.lineSeparator()
            + "│                 (annotation line 2)" + System.lineSeparator()
            + "╰─── last node" + System.lineSeparator();
        Assert.assertEquals(expected, actual);
    }



    @SuppressWarnings("ConstantConditions")
    private void annotateNodes(final TestNode pNode)
    {
        Assert.assertNotNull(pNode);
        Assert.assertNotNull(pNode.getText());
        if ("A".equals(pNode.getText())) {
            pNode.setAnnotation("annotation");
        }
        else if ("B2".equals(pNode.getText())) {
            TestNode b22 = new TestNode("This is a long node text" + System.lineSeparator() + "B22_xxxxxx");
            b22.setAnnotation("multi-line" + System.lineSeparator() + "annotation");
            pNode.addChild(b22);
        }
        else if ("B21".equals(pNode.getText())) {
            pNode.setAnnotation("multi-line" + System.lineSeparator() + "annotation");
        }
        else if ("C".equals(pNode.getText())) {
            pNode.setAnnotation("more multi-line" + System.lineSeparator() + "annotation");
        }
        else if ("C2".equals(pNode.getText())) {
            pNode.setAnnotation("annotation");
        }
        if (pNode.getChildren().iterator().hasNext()) {
            pNode.getChildren().forEach(this::annotateNodes);
        }
    }
}
