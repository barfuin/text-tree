/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.style;

import java.io.IOException;
import java.net.URISyntaxException;

import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.TestTreeFactory;
import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.junit.Assert;
import org.junit.Test;


/**
 * Unit test for tree styles with various indent values.
 */
public class IndentationTest
{
    @Test
    public void testNpm6()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        options.setStyle(TreeStyles.NPM6);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected6-npm6.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testNpm6Padding0()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        final TreeStyle npmStylePadding0 = new TreeStyle("+--", "| ", "`--");
        options.setStyle(npmStylePadding0);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected6-npm6-padding0.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testNpm6Padding3()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        final TreeStyle npmStylePadding3 = new TreeStyle("+--   ", "| ", "`--   ");
        options.setStyle(npmStylePadding3);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected6-npm6-padding3.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testLongIndent()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        final TreeStyle longIndentStyle = new TreeStyle("+--- ", "|      ", "`--- ");
        options.setStyle(longIndentStyle);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected7-longIndent.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testWinTree()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        options.setStyle(TreeStyles.WIN_TREE);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected8-wintree.txt");
        Assert.assertEquals(expected, actual);
    }
}
