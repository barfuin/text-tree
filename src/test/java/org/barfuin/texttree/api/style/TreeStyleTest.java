/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.style;

import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests of {@link TreeStyle}.
 */
public class TreeStyleTest
{
    @Test
    public void testMismatchedElements()
    {
        try {
            // the two junction arguments must be of equal length
            new TreeStyle("+---", "|   ", "invalidLastJunction", "<", ">");
            Assert.fail("expected exception was not thrown");
        }
        catch (IllegalArgumentException e) {
            // expected
            Assert.assertEquals("element size mismatch", e.getMessage());
        }
    }



    @Test
    public void testEmptyTextFragment1()
    {
        try {
            new TreeStyle("+---", "|   ", "");
            Assert.fail("expected exception was not thrown");
        }
        catch (IllegalArgumentException e) {
            // expected
            Assert.assertTrue(e.getMessage().contains("must not be null or empty"));
        }
    }



    @Test
    public void testEmptyTextFragment2()
    {
        try {
            new TreeStyle("+---", "", "`---");
            Assert.fail("expected exception was not thrown");
        }
        catch (IllegalArgumentException e) {
            // expected
            Assert.assertTrue(e.getMessage().contains("must not be null or empty"));
        }
    }



    @Test
    public void testEmptyTextFragment3()
    {
        try {
            new TreeStyle("", "|   ", "`---");
            Assert.fail("expected exception was not thrown");
        }
        catch (IllegalArgumentException e) {
            // expected
            Assert.assertTrue(e.getMessage().contains("must not be null or empty"));
        }
    }
}
