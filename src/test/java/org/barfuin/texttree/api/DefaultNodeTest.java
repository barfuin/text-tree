/*
 * Copyright 2020-2025 Barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

import java.util.Collections;
import java.util.Iterator;

import org.barfuin.texttree.api.color.NodeColor;
import org.junit.Assert;
import org.junit.Test;


/**
 * Simple unit tests of {@link DefaultNode}.
 */
public class DefaultNodeTest
{
    @Test
    public void testFullConstructor1()
    {
        final DefaultNode underTest = new DefaultNode("text", "key", NodeColor.Black, "annotation", null);

        Assert.assertEquals("text", underTest.getText());
        Assert.assertEquals("key", underTest.getKey());
        Assert.assertEquals(NodeColor.Black, underTest.getColor());
        Assert.assertEquals("annotation", underTest.getAnnotation());
        Assert.assertFalse(underTest.getChildren().iterator().hasNext());
    }



    @Test
    public void testFullConstructor2()
    {
        final DefaultNode underTest = new DefaultNode("text", "key", NodeColor.Black, null,
            Collections.singletonList(new DefaultNode()));

        Assert.assertEquals("text", underTest.getText());
        Assert.assertEquals("key", underTest.getKey());
        Assert.assertEquals(NodeColor.Black, underTest.getColor());
        Assert.assertNull(underTest.getAnnotation());

        Iterator<DefaultNode> children = underTest.getChildren().iterator();
        Assert.assertNotNull(children);
        Assert.assertTrue(children.hasNext());
        Assert.assertNotNull(children.next());
        Assert.assertFalse(children.hasNext());
    }



    @Test
    public void testSetters()
    {
        final DefaultNode underTest = new DefaultNode();
        underTest.setText("testText");
        underTest.setKey("testKey");
        underTest.setColor(NodeColor.DarkCyan);
        underTest.setAnnotation("testAnnotation");
        underTest.setAnnotationColor(NodeColor.LightGreen);
        underTest.addChild(new DefaultNode());

        Assert.assertEquals("testText", underTest.getText());
        Assert.assertEquals("testKey", underTest.getKey());
        Assert.assertEquals(NodeColor.DarkCyan, underTest.getColor());
        Assert.assertEquals("testAnnotation", underTest.getAnnotation());
        Assert.assertEquals(NodeColor.LightGreen, underTest.getAnnotationColor());

        Iterator<DefaultNode> children = underTest.getChildren().iterator();
        Assert.assertNotNull(children);
        Assert.assertTrue(children.hasNext());
        Assert.assertNotNull(children.next());
        Assert.assertFalse(children.hasNext());
    }



    @Test
    public void testSetChildrenNull()
    {
        final DefaultNode underTest = new DefaultNode();
        underTest.setChildren(null);

        Assert.assertEquals("", underTest.getText());
        Assert.assertEquals("", underTest.getKey());
        Assert.assertNull(underTest.getColor());
        Assert.assertNull(underTest.getAnnotation());

        Iterator<DefaultNode> children = underTest.getChildren().iterator();
        Assert.assertNotNull(children);
        Assert.assertFalse(children.hasNext());
    }



    @Test
    public void testSetChildren()
    {
        final DefaultNode underTest = new DefaultNode();
        underTest.setChildren(Collections.singletonList(new DefaultNode("child1")));

        Iterator<DefaultNode> children = underTest.getChildren().iterator();
        Assert.assertNotNull(children);
        Assert.assertTrue(children.hasNext());
        Node child = children.next();
        Assert.assertNotNull(child);
        Assert.assertEquals("child1", child.getText());
        Assert.assertEquals("child1", child.getKey());
        Assert.assertFalse(children.hasNext());
    }
}
