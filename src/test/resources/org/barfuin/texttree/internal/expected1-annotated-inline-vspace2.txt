ROOT
|
|
+--- A annotation
|
|
+--- B multi-line
|      annotation
|    |
|    |
|    +--- B1
|    |
|    |
|    +--- B2
|    |    |
|    |    |
|    |    `--- B21
|    |
|    |
|    `--- B3
|         |
|         |
|         +--- B31
|         |
|         |
|         `--- B32 more
|                multi-line
|                annotation
|
|
`--- C
     |
     |
     +--- C1
     |
     |
     `--- C2 annotation
